* Tarot Woman.
One of the creative coding example by deconbatch.
[[https://www.deconbatch.com/2019/12/tarot-woman.html]['Tarot Woman.' on Creative Coding : Examples with Code.]]
** Description.
A generative art made with Processing on Java programming language.

It draws the Vector Fields symmetrically and makes an interesting shape.
I changed shape factor parameters a little each symmetrical drawing.

    paramA  *= random(0.87, 1.15);

It gives some taste on the result.

** Change log.
   - created : 2019/12/07


