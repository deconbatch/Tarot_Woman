/**
 * Tarot Woman.
 * Symmetrical Vector Field drawing.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.12.07
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noStroke();
  noFill();

}

void draw() {

  float baseHue    = random(360.0);

  translate(width * 0.5, height * 0.5);
  background((baseHue + 90.0) % 360.0, 80.0, 20.0, 100);

  blendMode(BLEND);
  drawCanvas(baseHue + 90.0);

  pushMatrix();
  rotate(random(TWO_PI));
  blendMode(SCREEN);
  drawVectorField(baseHue);
  popMatrix();

  blendMode(BLEND);
  casing();

  saveFrame("frames/####.png");
  if (frameCount >= 3) {
    exit();
  }
}

/**
 * drawVectorField : draw Vector Field
 * @param  _baseHue : drawing color.
 */
void drawVectorField(float _baseHue) {

    // shape factors
  float paramA  = discreteRandom(0.3, 0.6, 2.5);
  float paramB  = discreteRandom(0.3, 0.6, 2.5);
  float paramC  = discreteRandom(0.05, -0.3, 0.3);
  float paramD  = discreteRandom(0.05, -0.3, 0.3);
  float plotDiv = random(0.0004, 0.0008);
  int   drawMax = floor(random(2.0, 6.0));
  int   plotMax = floor(10000 / drawMax / drawMax);

  float initMargin = 0.2;
  float initDiv    = 0.025;
  float baseSiz    = 2.0;
  float baseBri    = 100.0;

  for (int drawCnt = 0; drawCnt < drawMax; ++drawCnt) {

    rotate(TWO_PI * 1.0 / drawMax);
    // change shape factors a little
    paramA  *= random(0.87, 1.15);
    paramB  *= random(0.87, 1.15);
    paramC  *= random(0.87, 1.15);
    paramD  *= random(0.87, 1.15);

    // draw vector field
    for (float xInit = initMargin; xInit <= 1.0 - initMargin; xInit += initDiv) {
      for (float yInit = initMargin; yInit <= 1.0 - initMargin; yInit += initDiv) {
        float xPoint = xInit;
        float yPoint = yInit;
        for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

          float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);
          float sineRatio = sin(PI * plotRatio);
          float eHue      = _baseHue + plotRatio * 30.0 + (xInit + yInit) * 30.0;
          float eSat      = map(sineRatio, 0.0, 1.0, 100.0, 30.0);
          float eBri      = baseBri * (1.0 - sineRatio * 0.8);
          float eSiz      = baseSiz * sineRatio;

          float xPrev = xPoint;
          float yPrev = yPoint;
          xPoint += plotDiv * (sin(TWO_PI * paramA * yPrev) - cos(TWO_PI * paramC * xPrev));
          yPoint += plotDiv * (sin(TWO_PI * paramB * xPrev) - cos(TWO_PI * paramD * yPrev));

          fill(eHue % 360.0, eSat, eBri, (eSat + eBri) / 2.0);
          //          fill(eHue % 360.0, eSat, eBri, 100.0);
          ellipse(xPoint * width * 0.4, yPoint * height * 0.4, eSiz, eSiz);

        }
      }
    }

  }
}

/**
 * drawCanvas : draw sand wall
 * @param  _baseHue : sand color.
 */
void drawCanvas(float _baseHue) {
  for (int x = 0; x < width * 0.5; x += 2) {
    for (int y = 0; y < height * 0.5; y += 2) {
      float pSize = random(0.5, 1.0);
      float pDiv  = random(-2.0, 2.0);
      float pSat = 0.0;
      if ((x + y) % 3 == 0) {
        pSat = 100.0;
      }
      strokeWeight(pSize);
      stroke(_baseHue % 360.0, pSat, 40.0, 50.0);
      point(x + pDiv, y + pDiv);
      point(-x + pDiv, y + pDiv);
      point(x + pDiv, -y + pDiv);
      point(-x + pDiv, -y + pDiv);
    }
	}
  noStroke();
  noFill();
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  rectMode(CENTER);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
  noStroke();
}

/**
 * discreteRandom : returns min <= < max value
 * @param  _step : discrete step.
 * @param  _min  : _min <= return value.
 * @param  _max  : _max > return value.
 */
float discreteRandom(float _step, float _min, float _max) {
  return floor(random(1.0) * ((_max - _min) / _step)) * _step + _min;
}
